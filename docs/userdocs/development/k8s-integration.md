This page covers integrating GitLab with kubernetes cluster to automatically deploy from GitLab to kubernetes via CI/CD jobs.

1. In your project, go to `Operate -> Kubernetes clusters`, click the dropdown in the top right and select `Connect a cluster (certificate - deprecated)`
1. In the namespace create a gitlab service account: `kubectl create sa gitlab -n <your_namespace>`
1. Create the rolebinding for the service account:

        kubectl create -f - << EOF
        apiVersion: rbac.authorization.k8s.io/v1
        kind: RoleBinding
        metadata:
          name: gitlab
          namespace: <your_namespace>
        roleRef:
          apiGroup: rbac.authorization.k8s.io
          kind: ClusterRole
          name: admin
        subjects:
        - kind: ServiceAccount
          name: gitlab
          namespace: <your_namespace>
        EOF

1. Create a secret for the service account:

        kubectl -n <your_namespace> apply -f - << EOF
        apiVersion: v1
        kind: Secret
        metadata:
          name: gitlab-secret
          annotations:
            kubernetes.io/service-account.name: gitlab
        type: kubernetes.io/service-account-token
        EOF

1. Get the secret and CA for the service account:

      `kubectl get secret -n your_namespace | grep gitlab`

      `kubectl get secret -n your_namespace <gitlab-secret-...> -o yaml`

      `echo <the token value> | base64 -d` - this will give you the service token field value
      
      `echo <the CA value> | base64 -d` - CA

      API URL - get from your cluster config file (`https://67.58.53.147:6443`)

1. Uncheck `GitLab-managed cluster`, enter the namespace into `Project namespace prefix (optional, unique)`

1. Click `Add kubernetes cluster`

Now your cluster config will be available to tools like kubectl and helm to access your namespace. You can use [this project](https://gitlab.nrp-nautilus.io/prp/jupyterlab-west) as an example of how to automatically deploy a helm application yo your namespace and [this one](https://gitlab.nrp-nautilus.io/prp/nautilus-admission/-/blob/master/.gitlab-ci.yml#L28-39) to automatically update the deployment image.
