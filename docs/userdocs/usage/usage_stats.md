## Cluster usage by node and namespace

This is the experimental page for querying the monitoring information for nodes usage. Still work in progress. Data gathered since 09-15-23. Additional features, such as a downloadable table, can be viewed <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">here</a>.
<div id="observablehq-viewof-form-0037f4d3"></div>
<div id="observablehq-statusMessage-a97e9310"></div>
<div id="observablehq-viewof-gpuChart-0037f4d3"></div>
<div id="observablehq-viewof-cpuChart-0037f4d3"></div>
<div id="observablehq-viewof-memoryChart-0037f4d3"></div>
<div id="observablehq-viewof-storageChart-0037f4d3"></div>
<div id="observablehq-viewof-fpgaChart-0037f4d3"></div>
<p>Credit: <a href="https://observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace">Node Allocated Resources (By Namespace) by NRP Nautilus</a></p>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@observablehq/inspector@5/dist/inspector.css">
<style>#observablehq-viewof-form-0037f4d3 input { border: 1px solid black;}</style>
<script type="module">
import {Runtime, Inspector} from "https://cdn.jsdelivr.net/npm/@observablehq/runtime@5/dist/runtime.js";
import define from "https://api.observablehq.com/@nrp-nautilus/node-allocated-resources-by-namespace.js?v=3";
new Runtime().module(define, name => {
  if (name === "viewof form") return new Inspector(document.querySelector("#observablehq-viewof-form-0037f4d3"));
  if (name === "viewof gpuChart") return new Inspector(document.querySelector("#observablehq-viewof-gpuChart-0037f4d3"));
  if (name === "viewof cpuChart") return new Inspector(document.querySelector("#observablehq-viewof-cpuChart-0037f4d3"));
  if (name === "viewof memoryChart") return new Inspector(document.querySelector("#observablehq-viewof-memoryChart-0037f4d3"));
  if (name === "viewof storageChart") return new Inspector(document.querySelector("#observablehq-viewof-storageChart-0037f4d3"));
  if (name === "viewof fpgaChart") return new Inspector(document.querySelector("#observablehq-viewof-fpgaChart-0037f4d3"));
  if (name === "statusMessage") return new Inspector(document.querySelector("#observablehq-statusMessage-a97e9310"));
  return ["data","dataStatus","datatable","tick"].includes(name);
});
</script>